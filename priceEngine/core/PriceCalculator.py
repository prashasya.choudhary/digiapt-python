import string
from datetime import datetime

from priceEngine.models.Bicycle import Bicycle
from priceEngine.service.ChainAssemblyService import ChainAssemblyService
from priceEngine.service.FrameService import FrameService
from priceEngine.service.HandleService import HandleService
from priceEngine.service.SeatService import SeatService
from priceEngine.service.WheelService import WheelService


class PriceCalculator(Bicycle):
    bicycle: Bicycle = None

    def __init__(self, bicycle):
        super().__init__()
        self.bicycle: Bicycle = bicycle
        self.set_bicycle()

    def set_bicycle(self):
        date = self.bicycle.get_date()
        epoch_date = self.get_epoch_time(date)
        wheel_service: WheelService = WheelService(epoch_date, self.bicycle.get_wheel())
        wheel_service.calculate_price()
        frame_service: FrameService = FrameService(epoch_date, self.bicycle.get_frame())
        frame_service.calculate_price()
        handle_service: HandleService = HandleService(epoch_date, self.bicycle.get_handle())
        handle_service.calculate_price()
        chain_assembly_service: ChainAssemblyService = ChainAssemblyService(epoch_date,
                                                                            self.bicycle.get_chain_assembly())
        chain_assembly_service.calculate_price()
        seat_service: SeatService = SeatService(epoch_date, self.bicycle.get_seat())
        seat_service.calculate_price()
        print("price of this bicycle is: " + str(self.bicycle.seat.get_price() + self.bicycle.frame.get_price() +
                                                 self.bicycle.handle.get_price() + self.bicycle.wheel.get_price() +
                                                 self.bicycle.chainAssembly.get_price()))

    @staticmethod
    def get_epoch_time(date_in_string: string):
        formatter = '%d-%m-%Y'
        dt_obj = datetime.strptime(date_in_string, formatter)
        epoch_time = int(dt_obj.timestamp())
        return epoch_time
