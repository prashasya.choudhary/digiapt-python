from priceEngine.models.Bicycle import Bicycle
from priceEngine.core.PriceCalculator import PriceCalculator


class PricingEngine:
    bicycle: Bicycle = None

    def __init__(self, bicycle):
        self.bicycle = bicycle

    def calculate_price(self):
        PriceCalculator(self.bicycle)
