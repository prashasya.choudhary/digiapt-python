import json
import threading

from priceEngine.core.PriceEngine import PricingEngine
from priceEngine.models.Bicycle import Bicycle
from priceEngine.models.ChainAssembly import ChainAssembly
from priceEngine.models.Frame import Frame
from priceEngine.models.Handle import Handle
from priceEngine.models.Seat import Seat
from priceEngine.models.Wheel import Wheel


class EntryPoint:

    def __init__(self):
        self.take_inputs()

    @staticmethod
    def take_inputs():
        bicycle: Bicycle = Bicycle()
        wheel: Wheel = Wheel()
        seat: Seat = Seat()
        handle: Handle = Handle()
        frame: Frame = Frame()
        chain_assembly: ChainAssembly = ChainAssembly()

        type = input("Please enter M for manually or F for file : \n")
        if type == 'M':
            date = input("Please enter Date in DD-MM-YYYY format : \n")
            bicycle.set_date(date)
            wheel_spoke = input("Please enter wheel spoke type\n"
                                "ex: TANGENTIALLACING,WHEELBUILDING\n")
            wheel.set_spoke(wheel_spoke)
            wheel_rim = input("Please enter wheel rim type\n"
                              "ex: STEEL,ALLOY, CHROME\n")
            wheel.set_rim(wheel_rim)
            wheel_tube = input("Please enter wheel tube type\n"
                               "ex: TUBE,TUBELESS\n")
            wheel.set_tube(wheel_tube)
            wheel_tyre = input("Please enter wheel tyre type\n"
                               "ex: CLINCHER,TUBULAR\n")
            wheel.set_tyre(wheel_tyre)
            seat_type = input("Please enter wheel seat type\n"
                              "ex: COMFORT,ORDINARY,RACING\n")
            seat.set_type(seat_type)
            handle_type = input("Please enter wheel handle type\n"
                                "ex: DROP,ORDINARY,RANDONNEUR,STANDARD,TRACK\n")
            shock_lock = input("is shockLock required ? please enter Yes or No : \n")
            handle.set_type(handle_type)
            handle.set_shock_lock(shock_lock == "Yes")
            frame_type = input("Please enter wheel frame type\n"
                               "ex: CANTILEVER,DIAMOND,ORDINARY,PRONE,RECUMBENT,STEPTHROUGH\n")
            frame.set_type(frame_type)
            chain_size = int(input("Please enter wheel chain size in numbers\n"
                                   "values: 10,8,6\n"))
            chain_gear_speed = int(input("Please enter wheel chain gear speed in numbers\n"
                                         "values: 10,8,6\n"))
            chain_assembly.set_chain_size(chain_size)
            chain_assembly.set_speed(chain_gear_speed)
            bicycle.frame = frame
            bicycle.wheel = wheel
            bicycle.seat = seat
            bicycle.handle = handle
            bicycle.chainAssembly = chain_assembly

            pricing_engine: PricingEngine = PricingEngine(bicycle)
            pricing_engine.calculate_price()
        else:
            file = open('data.json')
            json_data = json.load(file)
            for obj in json_data:
                bicycle.set_date(obj['date'])
                wheel.set_spoke(obj['wheel']['spoke'])
                wheel.set_rim(obj['wheel']['rim'])
                wheel.set_tube(obj['wheel']['tube'])
                wheel.set_tyre(obj['wheel']['tyre'])
                seat.set_type(obj['seat']['type'])
                handle.set_type(obj['handle']['type'])
                handle.set_shock_lock(obj['handle']['shockLock'])
                frame.set_type(obj['frame']['type'])
                chain_assembly.set_chain_size(obj['chainAssembly']['chainSize'])
                chain_assembly.set_speed(obj['chainAssembly']['speed'])
                bicycle.frame = frame
                bicycle.wheel = wheel
                bicycle.seat = seat
                bicycle.handle = handle
                bicycle.chainAssembly = chain_assembly
                thread = threading.Thread(PricingEngine(bicycle).calculate_price())
                thread.start()


EntryPoint()
