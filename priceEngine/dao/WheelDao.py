from priceEngine.enum.Rim import Rim
from priceEngine.enum.Spoke import Spoke
from priceEngine.enum.Tube import Tube
from priceEngine.enum.Tyre import Tyre


class WheelDao:
    rim_steel_price = {1578240385: 2000.00, 1586102785: 1500.00}
    rim_alloy_price = {1578240385: 1000.00, 1586102785: 2000.00}
    rim_chrome_price = {1578240385: 2000.00, 1586102785: 1500.00}
    tangentlacing_spoke_price = {1578240385: 1000.00, 1586102785: 2000.00}
    wheelbuilding_spoke_price = {1578240385: 2000.00, 1586102785: 1500.00}
    tube_tube_price = {1578240385: 1000.00, 1586102785: 2000.00}
    tubeless_tube_price = {1578240385: 2000.00, 1586102785: 1500.00}
    clincher_tyre_price = {1578240385: 1000.00, 1586102785: 2000.00}
    tubular_tyre_price = {1578240385: 3000.00, 1586102785: 4000.00}

    def get_rim_price(self, part, date):
        price = 0
        if part == Rim.STEEL.value:
            for x, y in self.rim_steel_price.items():
                if x > date:
                    return price
                price = y
        elif part == Rim.ALLOY.value:
            for x, y in self.rim_alloy_price.items():
                if x > date:
                    return price
                price = y
        elif part == Rim.CHROME.value:
            for x, y in self.rim_chrome_price.items():
                if x > date:
                    return price
                price = y
        return price

    def get_spoke_price(self, part, date):
        price = 0
        if part == Spoke.TANGENTIALLACING.value:
            for x, y in self.tangentlacing_spoke_price.items():
                if x > date:
                    return price
                price = y
        elif part == Spoke.WHEELBUILDING.value:
            for x, y in self.wheelbuilding_spoke_price.items():
                if x > date:
                    return price
                price = y
        return price

    def get_tube_price(self, part, date):
        price = 0
        if part == Tube.TUBE.value:
            for x, y in self.tube_tube_price.items():
                if x > date:
                    return price
                price = y
        elif part == Tube.TUBELESS.value:
            for x, y in self.tubeless_tube_price.items():
                if x > date:
                    return price
                price = y
        return price

    def get_tyre_price(self, part, date):
        price = 0
        if part == Tyre.TUBULAR.value:
            for x, y in self.tubular_tyre_price.items():
                if x > date:
                    return price
                price = y
        elif part == Tyre.CLINCHER.value:
            for x, y in self.clincher_tyre_price.items():
                if x > date:
                    return price
                price = y
        return price
