class ChainAssemblyDao:
    chain_size_ten_price = {1578240385: 2222.00, 1586102785: 1111.00}
    chain_size_eight_price = {1578240385: 1000.00, 1586102785: 2000.00}
    chain_size_six_price = {1578240385: 1000.00, 1586102785: 2000.00}

    chain_speed_ten_price = {1578240385: 1000.00, 1586102785: 2000.00}
    chain_speed_eight_price = {1578240385: 1000.00, 1586102785: 2000.00}
    chain_speed_six_price = {1578240385: 100.00, 1586102785: 200.00, 1586132785: 250.00}

    def get_chain_size_price(self, part, date):
        price = 0
        if part == 10:
            for x, y in self.chain_size_ten_price.items():
                if x > date:
                    return price
                price = y
        elif part == 8:
            for x, y in self.chain_size_eight_price.items():
                if x > date:
                    return price
                price = y
        elif part == 6:
            for x, y in self.chain_speed_six_price.items():
                if x > date:
                    return price
                price = y
        return price

    def get_chain_speed_price(self, part, date):
        price = 0
        if part == 10:
            for x, y in self.chain_speed_ten_price.items():
                if x > date:
                    return price
                price = y
        elif part == 8:
            for x, y in self.chain_speed_eight_price.items():
                if x > date:
                    return price
                price = y
        elif part == 6:
            for x, y in self.chain_speed_six_price.items():
                if x > date:
                    return price
                price = y
        return price
