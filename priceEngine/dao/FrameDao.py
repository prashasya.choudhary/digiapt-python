from priceEngine.enum.FrameType import FrameType


class FrameDao:
    frame_ordinary_price = {1578240385: 2000.00, 1586102785: 1500.00}
    frame_cantilever_price = {1578240385: 1000.00, 1586102785: 2000.00}
    frame_diamond_price = {1578240385: 1000.00, 1586102785: 2000.00}
    frame_recumbent_price = {1578240385: 1000.00, 1586102785: 2000.00}
    frame_prone_price = {1578240385: 1000.00, 1586102785: 2000.00}
    frame_step_through_price = {1578240385: 1000.00, 1586102785: 2000.00}

    def get_frame_price(self, part, date):
        price = 0
        if part == FrameType.ORDINARY.value:
            for x, y in self.frame_ordinary_price.items():
                if x > date:
                    return price
                price = y
        elif part == FrameType.CANTILEVER.value:
            for x, y in self.frame_cantilever_price.items():
                if x > date:
                    return price
                price = y
        elif part == FrameType.DIAMOND.value:
            for x, y in self.frame_diamond_price.items():
                if x > date:
                    return price
                price = y
        elif part == FrameType.RECUMBENT.value:
            for x, y in self.frame_recumbent_price.items():
                if x > date:
                    return price
                price = y
        elif part == FrameType.PRONE.value:
            for x, y in self.frame_prone_price.items():
                if x > date:
                    return price
                price = y
        elif part == FrameType.STEPTHROUGH.value:
            for x, y in self.frame_step_through_price.items():
                if x > date:
                    return price
                price = y
        return price
