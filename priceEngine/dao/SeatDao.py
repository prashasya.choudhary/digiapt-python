from priceEngine.enum.SeatType import SeatType


class SeatDao:
    seat_comfort_price = {1578240385: 2000.00, 1586102785: 1500.00}
    seat_ordinary_price = {1578240385: 1000.00, 1586102785: 2000.00}
    seat_racing_price = {1578240385: 1000.00, 1586102785: 2000.00}

    def get_seat_price(self, part, date):
        price = 0
        if part == SeatType.ORDINARY.value:
            for x, y in self.seat_ordinary_price.items():
                if x > date:
                    return price
                price = y
        elif part == SeatType.COMFORT.value:
            for x, y in self.seat_comfort_price.items():
                if x > date:
                    return price
                price = y
        elif part == SeatType.RACING.value:
            for x, y in self.seat_racing_price.items():
                if x > date:
                    return price
                price = y
        return price
