from priceEngine.enum.HandleType import HandleType


class HandleDao:
    handle_ordinary_price = {1578240385: 2222.00, 1586102785: 1111.00}
    handle_drop_price = {1578240385: 1000.00, 1586102785: 2000.00}
    handle_randonneur_price = {1578240385: 1000.00, 1586102785: 2000.00}
    handle_standard_price = {1578240385: 1000.00, 1586102785: 2000.00}
    handle_track_price = {1578240385: 1000.00, 1586102785: 2000.00}
    handle_shocklock_price = {1578240385: 100.00, 1586102785: 200.00, 1586132785: 250.00}

    def get_handle_price(self, part, date):
        price = 0
        if part == HandleType.ORDINARY.value:
            for x, y in self.handle_ordinary_price.items():
                if x > date:
                    return price
                price = y
        elif part == HandleType.DROP.value:
            for x, y in self.handle_drop_price.items():
                if x > date:
                    return price
                price = y
        elif part == HandleType.RANDONNEUR.value:
            for x, y in self.handle_randonneur_price.items():
                if x > date:
                    return price
                price = y
        elif part == HandleType.STANDARD.value:
            for x, y in self.handle_standard_price.items():
                if x > date:
                    return price
                price = y
        elif part == HandleType.TRACK.value:
            for x, y in self.handle_track_price.items():
                if x > date:
                    return price
                price = y
        return price

    def get_shocklock_price(self, part, date):
        price = 0
        if part:
            for x, y in self.handle_shocklock_price.items():
                if x > date:
                    return price
                price = y
        return price
