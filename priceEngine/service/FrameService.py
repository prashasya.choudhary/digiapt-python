from priceEngine.models.Frame import Frame
from priceEngine.dao.FrameDao import FrameDao


class FrameService:
    date = None
    frame: Frame = None
    frameDao: FrameDao = None

    def __init__(self, date, frame):
        self.date = date
        self.frame = frame
        self.frameDao = FrameDao()

    def get_price(self):
        return self.frame.get_price()

    def calculate_price(self):
        self.frame.set_price(self.frameDao.get_frame_price(self.frame.get_type(), self.date))
