from priceEngine.models.ChainAssembly import ChainAssembly
from priceEngine.dao.ChainAssemblyDao import ChainAssemblyDao


class ChainAssemblyService:
    date = None
    chainAssembly: ChainAssembly = None
    chainAssemblyDao: ChainAssemblyDao = None

    def __init__(self, date, chain_assembly):
        self.date = date
        self.chainAssembly = chain_assembly
        self.chainAssemblyDao = ChainAssemblyDao()

    def get_price(self):
        return self.chainAssembly.get_price()

    def calculate_price(self):
        size_price = self.chainAssemblyDao.get_chain_size_price(self.chainAssembly.get_chain_size(), self.date)
        speed_price = self.chainAssemblyDao.get_chain_speed_price(self.chainAssembly.get_speed(), self.date)
        self.chainAssembly.set_price(size_price+speed_price)
