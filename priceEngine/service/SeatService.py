from priceEngine.models.Seat import Seat
from priceEngine.dao.SeatDao import SeatDao


class SeatService:
    date = None
    seat: Seat = None
    seatDao: SeatDao = None

    def __init__(self, date, seat):
        self.date = date
        self.seat = seat
        self.seatDao = SeatDao()

    def get_price(self):
        return self.seat.get_price()

    def calculate_price(self):
        self.seat.set_price(self.seatDao.get_seat_price(self.seat.get_type(), self.date))
