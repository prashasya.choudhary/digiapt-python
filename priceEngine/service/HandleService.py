from priceEngine.dao.HandleDao import HandleDao
from priceEngine.models.Handle import Handle


class HandleService:
    date = None
    handle: Handle = None
    handleDao: HandleDao = None

    def __init__(self, date, handle):
        self.date = date
        self.handle = handle
        self.handleDao = HandleDao()

    def get_price(self):
        return self.handle.get_price()

    def calculate_price(self):
        handle_price = self.handleDao.get_handle_price(self.handle.get_type(), self.date)
        shock_lock_price = self.handleDao.get_shocklock_price(self.handle.get_shock_lock(), self.date)
        self.handle.set_price(handle_price+shock_lock_price)
