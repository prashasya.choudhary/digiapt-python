from priceEngine.dao.WheelDao import WheelDao
from priceEngine.models.Wheel import Wheel


class WheelService:
    date = None
    wheel: Wheel = None
    wheelDao: WheelDao = None

    def __init__(self, date, wheel):
        self.date = date
        self.wheel = wheel
        self.wheelDao = WheelDao()

    def get_price(self):
        return self.wheel.get_price()

    def calculate_price(self):
        stoke_price = self.wheelDao.get_spoke_price(self.wheel.get_spoke(), self.date)
        rim_price = self.wheelDao.get_rim_price(self.wheel.get_rim(), self.date)
        tube_price = self.wheelDao.get_tube_price(self.wheel.get_tube(), self.date)
        tyre_price = self.wheelDao.get_tyre_price(self.wheel.get_tyre(), self.date)
        self.wheel.set_price(stoke_price + rim_price + tube_price + tyre_price)
