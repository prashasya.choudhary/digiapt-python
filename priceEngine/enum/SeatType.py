import enum


class SeatType(enum.Enum):
    COMFORT = "COMFORT"
    ORDINARY = "ORDINARY"
    RACING = "RACING"
