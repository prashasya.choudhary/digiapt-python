import enum


class HandleType(enum.Enum):
    DROP = "DROP"
    ORDINARY = "ORDINARY"
    RANDONNEUR = "RANDONNEUR"
    STANDARD = "STANDARD"
    TRACK = "TRACK"
