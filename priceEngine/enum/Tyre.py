import enum


class Tyre(enum.Enum):
    CLINCHER = "CLINCHER"
    TUBULAR = "TUBULAR"
