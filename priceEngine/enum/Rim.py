import enum


class Rim(enum.Enum):
    STEEL = "STEEL"
    ALLOY = "ALLOY"
    CHROME = "CHROME"
