import enum


class Tube(enum.Enum):
    TUBE = "TUBE"
    TUBELESS = "TUBELESS"
