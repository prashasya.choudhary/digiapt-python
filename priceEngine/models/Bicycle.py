from priceEngine.models.Wheel import Wheel
from priceEngine.models.Seat import Seat
from priceEngine.models.Handle import Handle
from priceEngine.models.Frame import Frame
from priceEngine.models.ChainAssembly import ChainAssembly


class Bicycle:

    def __init__(self):
        self.date = None
        self.wheel = Wheel()
        self.seat = Seat()
        self.handle = Handle()
        self.frame = Frame()
        self.chainAssembly = ChainAssembly()

    def set_date(self, date):
        self.date = date

    def get_date(self):
        return self.date

    def set_wheel(self, wheel):
        self.wheel = wheel

    def get_wheel(self):
        return self.wheel

    def set_seat(self, seat):
        self.seat = seat

    def get_seat(self):
        return self.seat

    def set_handle(self, handle):
        self.handle = handle

    def get_handle(self):
        return self.handle

    def set_frame(self, frame):
        self.frame = frame

    def get_frame(self):
        return self.frame

    def set_chain_assembly(self, chain_assembly):
        self.chainAssembly = chain_assembly

    def get_chain_assembly(self):
        return self.chainAssembly
