class Handle:
    def __init__(self):
        self.type = None
        self.shock_lock = None
        self.price = None

    def set_type(self, type):
        self.type = type

    def get_type(self):
        return self.type

    def set_shock_lock(self, shock_lock):
        self.shock_lock = shock_lock

    def get_shock_lock(self):
        return self.shock_lock

    def set_price(self, price):
        self.price = price

    def get_price(self):
        return self.price
