class Wheel:
    def __init__(self):
        self.spoke = None
        self.rim = None
        self.tube = None
        self.tyre = None
        self.price = None

    def set_spoke(self, spoke):
        self.spoke = spoke

    def get_spoke(self):
        return self.spoke

    def set_rim(self, rim):
        self.rim = rim

    def get_rim(self):
        return self.rim

    def set_tube(self, tube):
        self.tube = tube

    def get_tube(self):
        return self.tube

    def set_tyre(self, tyre):
        self.tyre = tyre

    def get_tyre(self):
        return self.tyre

    def set_price(self, price):
        self.price = price

    def get_price(self):
        return self.price
