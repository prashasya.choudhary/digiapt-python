class Frame:
    def __init__(self):
        self.type = None
        self.price = None

    def set_type(self, type):
        self.type = type

    def get_type(self):
        return self.type

    def set_price(self, price):
        self.price = price

    def get_price(self):
        return self.price
