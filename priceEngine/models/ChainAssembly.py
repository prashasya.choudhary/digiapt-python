class ChainAssembly:
    def __init__(self):
        self.chain_size = None
        self.speed = None
        self.price = None

    def set_chain_size(self, chain_size):
        self.chain_size = chain_size

    def get_chain_size(self):
        return self.chain_size

    def set_speed(self, speed):
        self.speed = speed

    def get_speed(self):
        return self.speed

    def set_price(self, price):
        self.price = price

    def get_price(self):
        return self.price
